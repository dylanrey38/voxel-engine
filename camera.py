from settings import *
from frustum import Frustum

class Camera:
    def __init__(self, position, yaw, pitch) -> None:
        self.position = glm.vec3(position)
        self.yaw = glm.radians(yaw)
        self.pitch = glm.radians(pitch)

        self.up = glm.vec3(0, 1, 0)
        self.right = glm.vec3(1, 0, 0)
        self.forward = glm.vec3(0, 0, -1)

        self.m_proj = glm.perspective(V_FOV, ASPECT_RATIO, NEAR, FAR)
        self.m_view = glm.mat4()

        self.frustum = Frustum(self)
    
    def update(self) -> None:
        self.update_vectors()
        self.update_view_matrix()
    
    def update_view_matrix(self) -> None:
        self.m_view = glm.lookAt(self.position, self.position + self.forward, self.up)
    
    def update_vectors(self):
        self.forward.x = glm.cos(self.yaw) * glm.cos(self.pitch)
        self.forward.y = glm.sin(self.pitch)
        self.forward.z = glm.sin(self.yaw) * glm.cos(self.pitch)

        self.forward = glm.normalize(self.forward)
        self.right = glm.normalize(glm.cross(self.forward, glm.vec3(0, 1, 0)))
        self.up = glm.normalize(glm.cross(self.right, self.forward))
    
    def rotate_pitch(self, delta_y) -> None:
        self.pitch -= delta_y
        self.pitch = glm.clamp(self.pitch, -PITCH_MAX, PITCH_MAX)
    
    def rotate_yaw(self, delta_x) -> None:
        self.yaw += delta_x
    
    def move_left(self, velocity) -> None:
        self.position -= self.right * velocity
    
    def move_right(self, velocity) -> None:
        self.position += self.right * velocity
    
    def move_up(self, velocity) -> None:
        self.position += self.up * velocity
    
    def move_down(self, velocity) -> None:
        self.position -= self.up * velocity

    def move_forward(self, velocity) -> None:
        self.position += self.forward * velocity
    
    def move_back(self, velocity) -> None:
        self.position -= self.forward * velocity
